/* mb.vapi
 *
 * Copyright (C) 2010 Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 */

namespace Musicbrainz {
	[Compact]
	[CCode (cprefix = "", free_function = "mb_Delete", cname  = "musicbrainz_t", cheader_filename = "musicbrainz/mb_c.h")]
	public class Mb {
		[CCode (cname = "mb_New")]
		public Mb ();
		[CCode (cname = "mb_UseUTF8")]
		public void use_utf8 (bool useit);
		[CCode (cname = "mb_GetVersion")]
		public void get_version (out int major, out int minor, out int rev);
		[CCode (cname = "mb_SetServer")]
		public int set_server (string address, short port);
		[CCode (cname = "mb_SetDebug")]
		public void set_debug (int debug);
		[CCode (cname = "mb_SetProxy")]
		public int set_proxy (string address, short port);
		[CCode (cname = "mb_SetDepth")]
		public void set_depth (int depth);
		[CCode (cname = "mb_Query")]
		public int query (string rdfquery);
		[CCode (cname = "mb_QueryWithArgs")]
		public int query_with_args (string rdfquery, [CCode (array_length = false, array_null_terminated = true)] string[] args);
		[CCode (cname = "mb_GetQueryError")]
		public void get_query_error (char[] error);
		[CCode (cname = "mb_Select1")]
		public void select1 (string selectquery, int ord);
		[CCode (cname = "mb_Select")]
		public void select (string selecquery);
		[CCode (cname ="mb_GetWebSubmitURL")]
		public void get_web_submit_url (char[] url);
		[CCode (cname = "mb_GetResultData")]
		public int get_result_data (string resultname, char[] data);
		[CCode (cname = "mb_GetResultData1")]
		public int get_result_data1 (string resultname, char[] data, int j);
		[CCode (cname = "mb_GetResultInt")]
		public int get_result_int (string resultname);
		[CCode (cname = "mb_GetIDFromURL")]
		public void get_id_from_url (string url, char[] id);
	}

	[CCodde (cheader_filename = "musicbrainz/queries.h")]
    public class Query {
		[CCode (cname = "MBQ_GetCDTOC")]
		public const string GET_CD_TOC;
		[CCode (cname = "MBQ_GetCDInfo")]
		public const string GET_CD_INFO;
    }

	[CCodde (cheader_filename = "musicbrainz/queries.h")]
	public class Extract {
		[CCode (cname = "MBE_TOCGetCDIndexId")]
		public const string TOC_GET_CD_INDEX_ID;
		[CCode (cname = "MBE_GetNumAlbums")]
		public const string GET_NUM_ALBUMS;
		[CCode (cname = "MBE_AlbumGetAlbumId")]
		public const string ALBUM_GET_ALBUM_ID;
		[CCode (cname = "MBE_AlbumGetAlbumArtistId")]
		public const string ALBUM_GET_ALBUM_ARTIST_ID;
		[CCode (cname = "MBE_AlbumGetAlbumArtistName")]
		public const string ALBUM_GET_ALBUM_ARTIST_NAME;
		[CCode (cname = "MBE_AlbumGetAlbumArtistSortName")]
		public const string ALBUM_GET_ALBUM_ARTIST_SORT_NAME;
		[CCode (cname = "MBE_AlbumGetAlbumName")]
		public const string ALBUM_GET_ALBUM_NAME;
		[CCode (cname = "MBE_AlbumGetAmazonAsin")]
		public const string ALBUM_GET_AMAZON_ASIN;
		[CCode (cname = "MBE_AlbumGetNumReleaseDates")]
		public const string ALBUM_GET_NUM_RELEASE_DATES;
		[CCode (cname = "MBE_ReleaseGetDate")]
		public const string RELEASE_GET_DATE;
		[CCode (cname = "MBE_AlbumGetNumTracks")]
		public const string ALBUM_GET_NUM_TRACKS;
		[CCode (cname = "MBE_AlbumGetTrackId")]
		public const string ALBUM_GET_TRACK_ID;
		[CCode (cname = "MBE_AlbumGetArtistId")]
		public const string ALBUM_GET_ARTIST_ID;
		[CCode (cname = "MBE_AlbumGetTrackName")]
		public const string ALBUM_GET_TRACK_NAME;
		[CCode (cname = "MBE_AlbumGetArtistName")]
		public const string ALBUM_GET_ARTIST_NAME;
		[CCode (cname = "MBE_AlbumGetArtistSortName")]
		public const string ALBUM_GET_ARTIST_SORT_NAME;
		[CCode (cname = "MBE_AlbumGetTrackDuration")]
		public const string ALBUM_GET_TRACK_DURATION;
	}

	[CCodde (cheader_filename = "musicbrainz/queries.h")]
    public class Select {
		[CCode (cname = "MBS_SelectAlbum")]
		public const string ALBUM;
		[CCode (cname = "MBS_SelectReleaseDate")]
		public const string RELEASE_DATE;
		[CCode (cname = "MBS_Back")]
		public const string BACK;
	}

	[CCodde (cheader_filename = "musicbrainz/queries.h")]
	public class ID {
		[CCode (cname = "MBI_VARIOUS_ARTIST_ID")]
		public const string VARIOUS_ARTIST;
	}
}
