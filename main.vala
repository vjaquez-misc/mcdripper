public class MCDRipper {
	private MainLoop loop;
	private List<Album> albums = null;
	private SourceFunc cb;
	private int current_duration;
	private int track_duration;

	private void run () {
		var metadata = new Metadata ();
		string url;
		albums = metadata.get_albums (out url);

		if (albums == null) {
			return;
		}

		loop = new MainLoop (null, false);

		do_rip.begin ();

		loop.run ();
	}

	private async bool do_rip () {
		cb = do_rip.callback;

		var ripper = new Ripper ();

		ripper.completion.connect (() => {
				close_print_track ();
				Idle.add (cb);
			});

		ripper.error.connect ((err) => {
				close_print_track ();
				stderr.printf ("\n\tError: %s\n", err.message);
				Idle.add (cb);
			});

		ripper.progress.connect ((secs) => {
				int previous_duration = current_duration;
				current_duration = (int) 10 * secs / track_duration;
				for (int i = 0; i < (current_duration - previous_duration); i++) {
					stderr.printf (".");
				}
			});

		File dir;
		foreach (var album in albums) {
			album.dump ();

			try {
				dir = make_directory (album);
			} catch {
				continue;
			}

			foreach (var track in album.tracks) {
				open_print_track (track);
				try {
					var file = build_file (track, dir);
					track_duration = track.duration;
					ripper.extract (track, file);
					yield;
				} catch (Error err) {
					print ("Error: %s", err.message);
					continue;
				}
			}
		}

		loop.quit ();

		return false;
	}

	private string sanitize_path (string path) {
		unowned string s = path;
		while (s.get_char () == '.') {
			s = s.next_char ();
		}

		string str = s;
		// replace separator with a hypen
		str.delimit ("/", '-');
		// replace separator with a hypen
		str.delimit ("\\:|", '-');
		// Replace all other weird characters to whitespace
		str.delimit ("*?&!\'\"$()`>{}[]<>", ' ');
		// Replace all whitespace with underscores
		str.delimit ("\t ", '_');

		string res;
		try {
			res = Filename.from_utf8 (str, -1, null, null);
		} catch {
			res = str;
		}

		return res;
	}

	private File make_directory (Album album) throws Error {
		var fdir = "./%s/%s".printf (album.artist, album.title);
		var dir = File.new_for_path (fdir);
		dir.make_directory_with_parents (null);
		return dir;
	}

	private File build_file (Track track, File dir) throws Error {
		var fname = sanitize_path ("%s - %s - %d - %s.m4a".printf (track.artist,
																   track.album.title,
																   track.number,
																   track.title));
		return dir.get_child (fname);
	}

	private void open_print_track (Track track) {
		string duration = "%d:%d".printf (track.duration / 60, track.duration % 60);
		stderr.printf ("[ %d - %s - %s ", track.number, track.title, duration);
	}

	private void close_print_track () {
		stderr.printf (" ]\n");
	}

	public static int main (string[] args) {
		Gst.init (ref args);

		var test = new MCDRipper ();
		test.run ();

		return 0;
	}
}
