/* mb.vapi
 *
 * Copyright (C) 2010 Víctor Manuel Jáquez Leal <vjaquez@igalia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 */

[CCode (lower_case_cprefix = "mb_", cheader_filename = "musicbrainz3/mb_c.h")]
namespace Musicbrainz3 {
	public void extract_uuid (string uri, char[] uuid);

	[Compact]
	[CCode (cprefix = "mb_webservice_", cname = "MbWebService")]
	public class WebService {
		public WebService ();
		public void set_host (string str);
		public void set_port (int port);
		public void set_path_prefix (string str);
		public void set_username (string str);
		public void set_password (string str);
		public void set_realm (string str);
		public void set_proxy_host (string str);
		public void set_proxy_username (string str);
		public void set_proxy_password (string str);
	}

	[Compact]
	[CCode (cprefix = "mb_disc_", cname = "MbDisc", free_function = "")]
	public class Disc {
		public void get_id (char[] str);
		public int get_sectors ();
		public int get_first_track_num ();
		[CCode (cname = "mb_read_disc")]
		public static Disc? read (string device_name);
		[CCode (cname = "mb_read_disc_with_error")]
		public static Disc? read_with_error (string device_name, char[] error);
		[CCode (cname = "mb_get_submission_url")]
		public void get_submission_url (string? host, int port, char[] str);
	}

	[Compact]
	[CCode (cprefix = "mb_query_", cname = "MbQuery", free_function = "mb_query_free")]
	public class Query {
		public Query (WebService? ws, string? cliend_id);
		public Artist? get_artist_by_id (string id, ArtistIncludes inc);
		public Release? get_release_by_id (string id, ReleaseIncludes inc);
		public Track? get_track_by_id (string id, TrackIncludes inc);
		public User? get_user_by_name (string name);
		public ResultList? get_artists (ArtistFilter flt);
		public ResultList? get_releases (ReleaseFilter flt);
		public ResultList? get_tracks (TrackFilter flt);
	}

	[Compact]
	[CCode (cprefix = "mb_result_list_", cname = "MbResultList", free_function = "mb_result_list_free")]
	public class ResultList {
		public int get_size ();
		public int get_score (int index);
		public Artist? get_artist (int index);
		public Release? get_release (int index);
		public Track? get_track (int index);
	}

	[Compact]
	[CCode (cprefix = "mb_artist_", cname = "MbArtist", free_function = "mb_artist_free")]
	public class Artist {
		public void get_id (char[] str);
		public void get_type (char[] str);
		public void get_name (char[] str);
		public void get_sortname (char[] str);
		public void get_disambiguation (char[] str);
		public void get_unique_name (char[] str);
		public void get_begin_date (char[] str);
		public void get_end_date (char[] str);
		public int get_num_aliases ();
		public unowned ArtistAlias get_alias (int index);
		public int get_num_releases ();
		public unowned Release get_release (int index);
		public int get_releases_offset ();
		public int get_releases_count ();
		public int get_num_relations ();
		public unowned Relation get_relation (int index);
	}

	[Compact]
	[CCode (cprefix = "mb_release_", cname = "MbRelease", free_function = "mb_release_free")]
	public class Release {
		public void get_id (char[] str);
		public void get_title (char[] str);
		public void get_text_language (char[] str);
		public void get_text_script (char[] str);
		public void get_asin (char[] str);
		public unowned Artist get_artist ();
		public int get_tracks_offset ();
		public int get_tracks_count ();
		public int get_num_relations ();
		public unowned Relation get_relation (int index);
		public int get_num_tracks ();
		public unowned Track get_track (int index);
		public int get_num_discs ();
		public unowned Disc get_disc (int index);
		public int get_num_release_events ();
		public unowned ReleaseEvent get_release_event (int index);
		public int get_num_types ();
		public void get_type (int index, char[] str);
	}

	[Compact]
	[CCode (cprefix = "mb_track_", cname = "MbTrack", free_function = "mb_track_free")]
	public class Track {
		public void get_id (char[] str);
		public void get_title (char[] str);
		public int get_duration ();
		public int get_num_relations ();
		public unowned Relation? get_relation (int index);
		public unowned Artist? get_artist ();
	}

	[Compact]
	[CCode (cprefix = "mb_artist_alias_", cname = "MbArtistAlias", free_function = "")]
	public class ArtistAlias {
		public void get_value (char[] str);
		public void get_script (char[] str);
	}

	[Compact]
	[CCode (cprefix = "mb_user_", cname = "MbUser", free_function = "mb_user_free")]
	public class User {
		public void get_name (char[] str);
		public void get_show_nag ();
		public int get_num_types ();
		public void get_type (int index, char[] str);
	}

	[SimpleType]
	[CCode (cname="void", free_function = "", ref_function =  "", unref_function = "", type_check_function = "")]
	public class Includes {
	}

	[Compact]
	[CCode (cprefix = "mb_artist_includes_", cname = "MbArtistIncludes", free_function = "mb_artist_includes_free", type_check_function = "")]
	public class ArtistIncludes : Includes {
		public ArtistIncludes ();
		public void aliases ();
		public void releases (string type);
		public void va_releases (string type);
		public void artist_relations ();
		public void release_relations ();
		public void track_relations ();
		public void url_relations ();
		public void release_events ();
	}

	[Compact]
	[CCode (cprefix = "mb_release_includes_", cname = "MbReleaseIncludes", free_function = "mb_release_includes_free", type_check_function = "")]
	public class ReleaseIncludes : Includes {
		public ReleaseIncludes ();
		public void artist ();
		public void counts ();
		public void discs ();
		public void tracks ();
		public void artist_relations ();
		public void release_relations ();
		public void track_relations ();
		public void url_relations ();
		public void release_events ();
	}

	[Compact]
	[CCode (cprefix = "mb_track_includes_", cname = "MbTrackIncludes", free_function = "mb_track_includes_free", type_check_function = "")]
	public class TrackIncludes : Includes {
		public TrackIncludes ();
		public void artists ();
		public void releases ();
		public void puids ();
		public void artist_relations ();
		public void track_relations ();
		public void url_relations ();
	}

	[Compact]
	[CCode (cprefix = "mb_artist_filter_", cname = "MbArtistFilter", free_function = "mb_artist_filter_free")]
	public class ArtistFilter {
		public ArtistFilter ();
		public void name (string value);
		public void limit (int value);
	}

	[Compact]
	[CCode (cprefix = "mb_release_filter_", cname = "MbReleaseFilter", free_function = "mb_release_filter_free")]
	public class ReleaseFilter {
		public ReleaseFilter ();
		public void title (string value);
		public void disc_id (string value);
		public void release_type (string value);
		public void artist_name (string value);
		public void artist_id (string value);
		public void limit (int value);
	}

	[Compact]
	[CCode (cprefix = "mb_track_filter_", cname = "MbTrackFilter", free_function = "mb_track_filter_free")]
	public class TrackFilter {
		public TrackFilter ();
		public void title (string value);
		public void artist_name (string value);
		public void artist_id (string value);
		public void release_title (string value);
		public void release_id (string value);
		public void duration (int value);
		public void puid (string value);
		public void limit (int value);
	}

	[Compact]
	[CCode (cprefix = "mb_user_filter_", cname = "MbUserFilter", free_function = "mb_user_filter_free")]
	public class UserFilter {
		public UserFilter ();
		public void name (string value);
	}

	[Compact]
	[CCode (cprefix = "mb_release_event_", cname = "MbReleaseEvent", free_function = "")]
	public class ReleaseEvent {
		public void get_country (char[] str);
		public void get_date (char[] str);
	}

	[Compact]
	[CCode (cprefix = "mb_relation_", cname = "MbRelation", free_function = "")]
	public class Relation {
		public void get_type (char[] str);
		public void get_target_id (char[] str);
		public void get_target_type (char[] str);
		public void get_begin_date (char[] str);
		public void get_end_date (char[] str);
		public int get_direction ();
		public unowned Entity get_target ();
		public int get_num_attributes ();
		public void get_attribute (int index, char[] str);
	}

	[Compact]
	[CCode (cname = "MbEntity", free_function = "")]
	public class Entity {
	}
}