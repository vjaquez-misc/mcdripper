VALAC = valac

MB_PKG = --pkg=libmusicbrainz
MB3_PKG = --pkg=libmusicbrainz3 --pkg=gstreamer-0.10 --pkg=gio-2.0

SOURCES = metadata.vala ripper.vala main.vala

all: mcdripper

mb2-test: mb2-test.vala
	$(VALAC) --vapidir=./ $(MB_PKG) -o $@ $^

findartist: find-artist.vala
	$(VALAC) -g --vapidir=./ $(MB3_PKG) -o $@ $^

mcdripper: $(SOURCES)
	$(VALAC) -g --vapidir=./ $(MB3_PKG) -o $@ $^

clean:
	@rm mb2-test findartist mcdripper
