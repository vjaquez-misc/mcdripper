using Musicbrainz3;

public class FindArtist {
	public static int main (string[] args) {
		if (args.length < 2) {
			stderr.printf ("Usage: findartist 'artist name'");
			return -1;
		}

		var f = new ArtistFilter ();
		f.name (args[1]);
		f.limit (5);
		var q = new Query (null, null);
		var results = q.get_artists (f);
		char buffer[1024];

		for (int i = 0; i < results.get_size (); i++) {
			var artist = results.get_artist (i);
			stdout.printf ("Score: %d\n", results.get_score (i));
			artist.get_id (buffer);
			stdout.printf ("Id: %s\n", (string) buffer);
			artist.get_name (buffer);
			stdout.printf ("Name: %s\n", (string) buffer);
			artist.get_sortname(buffer);
			stdout.printf ("SortName: %s\n", (string) buffer);
			stdout.printf ("\n");
		}

		return 0;
	}
}