/**
 * valac --vapidir=./ --pkg=libmusicbrainz mb2-test.vala  -X -lmusicbrainz
 */

using Musicbrainz;

public class MbTest : Object {
	private int get_cd_metadata (out string url) {
		var mb = new Musicbrainz.Mb ();

		mb.use_utf8 (true);
		if (GLib.Environment.get_variable ("MB_DEBUG") != null) {
			mb.set_debug (7);
		}

		char data[1024];

		if (mb.query (Musicbrainz.Query.GET_CD_INFO) == 0) {
			mb.get_query_error (data);
			print ("This CD could not be queried: %s\n", (string) data);
		}

		mb.get_web_submit_url (data);
		url = (string) data;
		print ("URL: %s\n", url);

		var num_albums = mb.get_result_int (Musicbrainz.Extract.GET_NUM_ALBUMS);
		if (num_albums < 1) {
			return -1;
		}

		for (int i = 1; i <= num_albums; i++) {
			bool from_freedb = false;
			print ("\n");
			mb.select1 (Musicbrainz.Select.ALBUM, i);
			if (mb.get_result_data (Musicbrainz.Extract.ALBUM_GET_ALBUM_ID, data) != 0) {
				string album_url = (string) data;
				from_freedb = album_url.str ("freedb:") == album_url;
				mb.get_id_from_url ((string) data, data);
				print ("\tAlbum id = %s\n", (string) data);
			}

			if (mb.get_result_data (Musicbrainz.Extract.ALBUM_GET_ALBUM_ARTIST_ID, data) != 0) {
				mb.get_id_from_url ((string) data, data);
				string artist_id = (string) data;
				print ("\tArtist id = %s\n", artist_id);

				if (mb.get_result_data (Musicbrainz.Extract.ALBUM_GET_ALBUM_ARTIST_NAME, data) != 0) {
					print ("\tArtist = %s\n", (string) data);
				} else {
					if (artist_id == Musicbrainz.ID.VARIOUS_ARTIST) {
						print ("\tArtist = %s\n", "Various");
					} else {
						print ("\tArtist = %s\n", "Unknown Artist");
					}
				}

				if (mb.get_result_data (Musicbrainz.Extract.ALBUM_GET_ALBUM_ARTIST_SORT_NAME, data) != 0) {
					print ("\tArtist sort name = %s\n", (string) data);
				}
			}

			if (mb.get_result_data (Musicbrainz.Extract.ALBUM_GET_ALBUM_NAME, data) != 0) {
				print ("\tAlbum Title = %s\n", (string) data);
			} else {
				print ("\tUknown Title");
			}

			if (mb.get_result_data (Musicbrainz.Extract.ALBUM_GET_AMAZON_ASIN, data) != 0) {
				print ("\tAsin = %s\n", (string) data);
			}

			{
				int num_releases = mb.get_result_int (Musicbrainz.Extract.ALBUM_GET_NUM_RELEASE_DATES);
				if (num_releases > 0) {
					mb.select1 (Musicbrainz.Select.RELEASE_DATE, 1);
					if (mb.get_result_data (Musicbrainz.Extract.RELEASE_GET_DATE, data) != 0) {
						print ("\tRelease date = %s\n", (string) data);
					}
					mb.select (Musicbrainz.Select.BACK);
				}
			}

			int num_tracks = mb.get_result_int (Musicbrainz.Extract.ALBUM_GET_NUM_TRACKS);
			if (num_tracks < 1) {
				warning ("Incomplete metadata for this CD");
				return -1;
			}

			for (int j = 1; j <= num_tracks; j++) {
				print ("\t\tTrack = %d\n", j);

				if (mb.get_result_data1 (Musicbrainz.Extract.ALBUM_GET_TRACK_ID, data, j) != 0) {
					mb.get_id_from_url ((string) data, data);
					print ("\t\tTrack id = %s\n", (string) data);
				}

				if (mb.get_result_data1 (Musicbrainz.Extract.ALBUM_GET_ARTIST_ID, data, j) != 0) {
					mb.get_id_from_url ((string) data, data);
					print ("\t\tArtist id = %s\n", (string) data);
				}

				if (mb.get_result_data1 (Musicbrainz.Extract.ALBUM_GET_TRACK_NAME, data, j) != 0) {
					print ("\t\tTitle = %s\n", (string) data);
				} else {
					print ("\t\tUntitled\n");
				}

				if (mb.get_result_data1 (Musicbrainz.Extract.ALBUM_GET_ARTIST_NAME, data, j) != 0) {
					print ("\t\tArtist name = %s\n", (string) data);
				}

				if (mb.get_result_data1 (Musicbrainz.Extract.ALBUM_GET_ARTIST_SORT_NAME, data, j) != 0) {
					print ("\t\tArtist sort name = %s\n", (string) data);
				}

				if (mb.get_result_data1 (Musicbrainz.Extract.ALBUM_GET_TRACK_DURATION, data, j) != 0) {
					print ("\t\tDuration = %s\n", (string) data);
				}
			}
		}

		return 0;
	}

	public void run () {
		string url;
		if (get_cd_metadata (out url) != 0) {
			print ("Can't get CD metadata");
		}
	}

	static int main (string[] args) {
		var test = new MbTest ();
		test.run ();

		return 0;
	}
}

