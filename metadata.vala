using Musicbrainz3;

public class Track : Object {
	public Album album;
	public int number; /* track number */
	public string? title;
	public string? artist;
	public string? artist_sortname;
	public int duration; /* seconds */
	public string? track_id;
	public string? artist_id;

	public Track (Release release, Album album, int number) {
		char buffer[1024];
		char uuid_buffer[1024];

		unowned Musicbrainz3.Track mbt = release.get_track (number);

		this.album = album;
		this.number = number + 1;

		mbt.get_id (buffer);
		Musicbrainz3.extract_uuid ((string ) buffer, uuid_buffer);
		track_id = (string) uuid_buffer;

		mbt.get_title (buffer);
		title = (string) buffer;

		duration = mbt.get_duration () / 1000;

		unowned Artist artist = mbt.get_artist ();
		if (artist == null) {
			artist = release.get_artist ();
		}
		artist.get_id (buffer);
		Musicbrainz3.extract_uuid ((string) buffer, uuid_buffer);
		artist_id = (string) uuid_buffer;

		artist.get_name (buffer);
		this.artist = (string) buffer;

		artist.get_sortname (buffer);
		artist_sortname = (string) buffer;
	}

	public void dump () {
		print ("\t[ %d - %s - %d ]\n", number, title, duration);
	}
}

public class Album : Object {
	public string? title;
	public string? artist;
	public string? artist_sortname;
	public string? genre;
	public int number; /* number of tracks in the album */
	public int disc_number;
	public List<Track>? tracks = null;
	public Date? release_date;
	public string? album_id;
	public string? artist_id;
	public string? asin;
	public string? discogs = null;
	public string? wikipedia = null;

	private Date? scan_date (string date) {
		int year = 1, month = 1, day = 1;
		var matched = date.scanf ("%u-%u-%u", &year, &month, &day);
		if (matched >= 1) {
			if (day == 0) {
				day = 1;
			}

			if (month == 0) {
				month = 1;
			}

			var retval = Date ();
			retval.set_dmy ((DateDay) day, month, (DateYear) year);
			return retval;
		}

		return null;
	}

	public Album (Release release) {
		char buffer[1024];
		char uuid_buffer[1024];

		release.get_id (buffer);
		Musicbrainz3.extract_uuid ((string) buffer, uuid_buffer);
		album_id = (string) uuid_buffer;

		release.get_title (buffer);
		title = (string) buffer;

		unowned Artist artist = release.get_artist ();

		artist.get_id (buffer);
		Musicbrainz3.extract_uuid ((string) buffer, uuid_buffer);
		artist_id = (string) uuid_buffer;

		artist.get_name (buffer);
		this.artist = (string) buffer;

		artist.get_sortname (buffer);
		artist_sortname = (string) buffer;

		if (release.get_num_release_events () >= 1) {
			unowned ReleaseEvent revent = release.get_release_event (0);
			revent.get_date (buffer);
			release_date = scan_date ((string) buffer);
		}

		number = release.get_num_tracks ();

		release.get_asin (buffer);
		asin = (string) buffer;

		for (int i = 0; i < release.get_num_relations (); i++) {
			unowned Relation relation = release.get_relation (i);
			relation.get_type (buffer);
			string type = (string) buffer;
			if (type != null && type == "http://musicbrainz.org/ns/rel-1.0#Wikipedia") {
				relation.get_target_id (buffer);
				wikipedia = (string) buffer;
			} else if (type != null && type == "http://musicbrainz.org/ns/rel-1.0#Wikipedia") {
				relation.get_target_id (buffer);
				discogs = (string) buffer;
				continue;
			}
		}

		for (int i = 0; i < number; i++) {
			var track = new Track (release, this, i);
			tracks.append (track);
		}
	}

	public void dump () {
		char strtime[1025];
		if (release_date != null) {
			release_date.strftime (strtime, "%d-%m-%y");
		}
		print ("[ %s / %s ~ %s ]\n", artist, title, (string) strtime);
	}
}

public class Metadata : Object {
	public string device { get; construct; default = "/dev/cdrom"; }

	private Disc disc;
	private WebService ws = new WebService ();

	private ReleaseIncludes get_release_includes () {
		Includes includes = new ReleaseIncludes ();
		(includes as ReleaseIncludes).artist ();
		(includes as ReleaseIncludes).tracks ();
		(includes as ArtistIncludes).release_events ();
		(includes as TrackIncludes).url_relations ();
		return (includes as ReleaseIncludes);
	}

	public List<Album>? get_albums (out string url) {
		char buffer[1024];
		List? albums = null;

		disc = Disc.read_with_error (device, buffer);
		if (disc == null) {
			print ("error read failed: %s\n", (string) buffer);
			return null;
		}

		disc.get_submission_url (null, 0, buffer);
		url = (string) buffer;

		disc.get_id (buffer);
		string id = (string) buffer;

		var query = new Query (ws, "mcdripper");

		var filter = new ReleaseFilter ();
		filter.disc_id (id);
		var results = query.get_releases (filter);
		if (results == null) {
			return null;
		}

		for (int i = 0; i < results.get_size (); i++) {
			var release = results.get_release (i);
			if (release != null) {
				release.get_id (buffer);
				var includes = get_release_includes ();
				release = query.get_release_by_id ((string) buffer, includes);
				if (release != null) {
					var album = new Album (release);
					albums.append (album);
				}
			}
		}

		return albums;
	}
}