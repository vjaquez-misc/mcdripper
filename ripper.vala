using Gst;

public errordomain IOError {
	INTERNAL_ERROR
}

public class Ripper : GLib.Object {
	private dynamic Element? pipeline = null;
	private dynamic Element? cdsrc = null;
	private dynamic Element? filesink = null;
	private bool rebuild_pipeline = true;
	private Format format = Gst.Format.TIME;
	private uint tick_id = 0;

	public string device { set; get; default = "/dev/cdrom"; }
	public int paranoia_mode { set; get; default = 0x08; }

	public signal void progress (int i);
	public signal void completion ();
	public signal void error (Error err);

	private void build_pipeline () throws IOError {
		pipeline = new Pipeline ("pipeline");
		var bus = pipeline.get_bus ();
		bus.add_signal_watch ();

		bus.message["error"].connect (error_cb);

		cdsrc = Element.make_from_uri (URIType.SRC, "cdda://1", "cd_src");
		if (cdsrc == null) {
			throw new IOError.INTERNAL_ERROR ("Could not create GStreamer CD reader");
		}
		cdsrc.device = device;
		cdsrc.paranoia_mode = paranoia_mode;
		cdsrc.read_speed = int.MAX;

		dynamic Element queue = ElementFactory.make ("queue", "queue");
		queue.max_size_time = (uint64) 120 * SECOND;

		dynamic Element encoder;
		try {
			encoder = parse_bin_from_description ("audioresample ! audioconvert ! faac ! qtmux", true);
		} catch (Error err) {
			throw new IOError.INTERNAL_ERROR ("Could not create GStreamer encoder: %s", err.message);
		}

		bus.message["eos"].connect (eos_cb);

		filesink = ElementFactory.make ("giosink", "filesink");
		if (filesink == null) {
			throw new IOError.INTERNAL_ERROR ("Could not create GStreamer file output");
		}

		(pipeline as Bin).add_many (cdsrc, queue, encoder, filesink);
		if (!cdsrc.link_many (queue, encoder, filesink)) {
			throw new IOError.INTERNAL_ERROR ("Could not link pipeline");
		}

		rebuild_pipeline = false;
	}

	private bool tick_cb () {
		State state;
		State pending_state;

		pipeline.get_state (out state, out pending_state, 0);
		if (state != State.PLAYING && pending_state != State.PLAYING) {
			return false;
		}

		int64 nanos;
		if (cdsrc.query_position (ref format, out nanos) == false) {
			warning ("Could not get current track position");
			return true;
		}

		int secs = (int) nanos / SECOND;
		progress (secs);

		return true;
	}

	public void cancel () {
		State state;
		pipeline.get_state (out state, null, (ClockTime) CLOCK_TIME_NONE);

		if (state != State.PLAYING) {
			return;
		}

		pipeline.set_state (State.NULL);
		rebuild_pipeline = true;
	}

	public void extract (Track track, File file) throws Error {
		if (rebuild_pipeline == true) {
			build_pipeline ();
		}

		pipeline.set_state (State.NULL);
		filesink.file = file;

		var iter = (pipeline as Bin).iterate_all_by_interface (typeof (TagSetter));
		var done = false;
		while (done != true) {
			void* elem;
			switch (iter.next (out elem)) {
			case IteratorResult.OK:
				TagSetter tagger = (TagSetter) elem;
				tagger.add_tags (TagMergeMode.REPLACE_ALL,
								 TAG_TITLE, track.title,
								 TAG_ARTIST, track.artist,
								 TAG_TRACK_NUMBER, track.number,
								 TAG_TRACK_COUNT, track.album.number,
								 TAG_ALBUM, track.album.title,
								 TAG_DURATION, track.duration * SECOND);

				if (track.artist_sortname != null && track.artist_sortname != "") {
					tagger.add_tags (TagMergeMode.APPEND,
									 TAG_ARTIST_SORTNAME, track.artist_sortname);
				}

				if (track.album.genre != null && track.album.genre != "") {
					var values = track.album.genre.split (",");
					foreach (string l in values) {
						l.strip ();
						tagger.add_tags (TagMergeMode.APPEND, TAG_GENRE, l);
					}
				}

				if (track.album.release_date != null) {
					tagger.add_tags (TagMergeMode.APPEND,
									 TAG_DATE, track.album.release_date);
				}

				if (track.album.disc_number > 0) {
					tagger.add_tags (TagMergeMode.APPEND,
									 TAG_ALBUM_VOLUME_NUMBER, track.album.disc_number);
				}

				break;
			case IteratorResult.RESYNC:
				warning ("Got GST_ITERATOR_RESYNC, not sure what to do");
				iter.resync ();
				break;
			case IteratorResult.ERROR:
				done = true;
				break;
			case IteratorResult.DONE:
				done = true;
				break;
			}
		}

		cdsrc.track = track.number;

		var state_ret = pipeline.set_state (State.PLAYING);

		if (state_ret == StateChangeReturn.ASYNC) {
			// Wait for state change to either complete o fail, but not for too long,
			// just to catch inmmediate errors. The rest we'll handle asynchronously
			state_ret = pipeline.get_state (null, null, (ClockTime) SECOND / 2);
		}

		if (state_ret == StateChangeReturn.FAILURE) {
			Error err = null;
			var msg = pipeline.bus.poll (MessageType.ERROR, 0);

			if (msg != null) {
				msg.parse_error (out err, null);
			} else {
				err = new IOError.INTERNAL_ERROR ("Error starting ripping pipeline");
			}

			pipeline.set_state (State.NULL);
			rebuild_pipeline = true;

			throw err;
		}

		tick_id = Timeout.add (250, tick_cb);
	}

	private void error_cb (Message message) {
		pipeline.set_state (State.NULL);
		rebuild_pipeline = true;

		if (tick_id != 0) {
			Source.remove (tick_id);
			tick_id = 0;
		}

		Error err;
		message.parse_error (out err, null);
		error (err);
	}

	private void eos_cb (Message message) {
		pipeline.set_state (State.NULL);

		if (tick_id != 0) {
			Source.remove (tick_id);
			tick_id = 0;
		}

		completion ();
	}
}